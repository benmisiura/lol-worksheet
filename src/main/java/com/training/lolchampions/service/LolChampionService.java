package com.training.lolchampions.service;

import com.training.lolchampions.entity.LolChampion;
import com.training.lolchampions.repository.LolChampionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LolChampionService {

    @Autowired
    LolChampionRepository lolChampionRepository;

    public List<LolChampion> findAll() {
        return this.lolChampionRepository.findAll();
    }

    public LolChampion save(LolChampion lolChampion) {
        return this.lolChampionRepository.save(lolChampion);
    }
}
