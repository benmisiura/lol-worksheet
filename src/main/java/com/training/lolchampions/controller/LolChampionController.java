package com.training.lolchampions.controller;

import com.training.lolchampions.entity.LolChampion;
import com.training.lolchampions.service.LolChampionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/lolchampion")
public class LolChampionController {

    private static final Logger LOG = LoggerFactory.getLogger(LolChampionController.class);

    @Autowired
    private LolChampionService lolChampionService;

    @GetMapping
    public List<LolChampion> findAll() {
        return this.lolChampionService.findAll();
    }

    @PostMapping
    public LolChampion save(@RequestBody LolChampion lolChampion) {
        LOG.debug("request to create lolChampion [" + lolChampion + "]");
        return this.lolChampionService.save(lolChampion);
    }
}
