package com.training.lolchampions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LolworksheetApplication {

    public static void main(String[] args) {
        SpringApplication.run(LolworksheetApplication.class, args);
    }

}
